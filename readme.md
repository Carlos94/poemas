## Pasos para ejecutar el proyecto

- Clonar el proyecto en la carpeta htdocs de xampp.
- Abrir un terminal o simbolo del sistema ubicado en la ruta de la carpeta.
- Escribir <strong> php artisan serve </strong>.
- Abrir el navegador en la ruta <strong> http://localhost:8000 </strong>.
- Cargar un archivo .txt con la estructura del ejemplo enviado.
