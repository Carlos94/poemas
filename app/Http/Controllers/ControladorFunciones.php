<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControladorFunciones extends Controller
{
    public $numLineas = "";
    public $regla = [];
    public $abjetive = "";
    public $noun = "";
    public $pronoun = "";
    public $verb = "";
    public $preposition = "";


    public function cargarFormulario(){
        return view('welcome');
    }

    public function leerArchivo(Request $request){

        if (file_exists($request['archivo'])){
            $texto = fopen($request['archivo'], "r") or die("Error");

            while (!feof($texto)){
                $linea = fgets($texto);

                //Cantidad de lineas para el poema
                if (strpos($linea," <LINE>") ){
                    $this->numLineas = $this->cantidadLineas($linea);
                }

                //saber la composicion de una linea
                if (strpos($linea,"<PRONOUN> ") ){
                    $this->regla = $this->crearLinea($linea);
                }

                //array de abjetives
                if (strpos($linea,"<NOUN>|<ADJECTIVE>|") ){
                    $this->abjetive = $linea;
                }

                //array de noun
                if (strpos($linea,"<VERB>|<PREPOSITION>") ){
                    $this->noun = $linea;
                }

                //array de PRONOUN
                if (preg_match('/PRONOUN:/i', $linea)){
                    $this->pronoun = $linea;
                }

                //array de VERB
                if (strpos($linea," <PREPOSITION>|<PRONOUN>") ){
                    $this->verb = $linea;
                }

                //array de PREPOSITION
                if (strpos($linea," <NOUN>|<PRONOUN>|<ADJECTIVE>") ){
                    $this->preposition = $linea;
                }
            }

            $this->lineaPoema($this->regla, $this->numLineas);
        }else{
            dd("archivo no encontrado");
        }
    }

    //busco en el archivo el numero de lineas que va a tener mi poema partiendo de la regla POEM
    public function cantidadLineas($argumento){
        $arrayCantidad = [];
        $cantidad = "";

        $aux = str_replace("\r\n","", $argumento);
        $aux = str_replace("POEM: ","", $aux);
        $aux = $aux." ";

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ' '){
                $cantidad = $cantidad.$aux[$i];
            }else{
                array_push($arrayCantidad, $cantidad);
                $cantidad ="";
            }
        }

        return count($arrayCantidad);
    }

    public function abjetivoRandom(){
        $arrayAbjetivos = [];
        $abjetivo = "";
        $aux = str_replace("|",",", $this->abjetive);
        $aux = str_replace("ADJECTIVE: ","",$aux);
        $palabraClave = " <NOUN>,<ADJECTIVE>,"."$"."END";
        $aux = str_replace($palabraClave,"",$aux);
        $aux = $aux.",";

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ','){
                $abjetivo = $abjetivo.$aux[$i];
            }else{
                array_push($arrayAbjetivos, $abjetivo);
                $abjetivo ="";
            }
        }
        $random=rand(0,count($arrayAbjetivos)-1);
        return $arrayAbjetivos[$random];
    }

    public function nounRandom(){
        $arrayNoun = [];
        $noun = "";
        $aux = str_replace("|",",", $this->noun);
        $aux = str_replace("NOUN: ","",$aux);
        $palabrasClave = " <VERB>,<PREPOSITION>,"."$"."END";
        $aux = str_replace($palabrasClave,"",$aux);
        $aux = $aux."," ;

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ','){
                $noun = $noun.$aux[$i];
            }else{
                array_push($arrayNoun, $noun);
                $noun ="";
            }
        }
        $random=rand(0,count($arrayNoun)-1);

        return $arrayNoun[$random];
    }

    public function verbRandom(){
        $arrayVerb = [];
        $verb="";

        $aux = str_replace("|",",", $this->verb);
        $aux = str_replace("VERB: ","",$aux);
        $palabrasClave = " <PREPOSITION>,<PRONOUN>,"."$"."END";
        $aux = str_replace($palabrasClave,"",$aux);
        $aux = $aux."," ;

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ','){
                $verb = $verb.$aux[$i];
            }else{
                array_push($arrayVerb, $verb);
                $verb ="";
            }
        }
        $random=rand(0,count($arrayVerb)-1);

        return $arrayVerb[$random];
    }

    public function prepositionRandom(){
        $arrayPreposition = [];
        $preposition="";

        $aux = str_replace("|",",", $this->preposition);
        $aux = str_replace("PREPOSITION: ","",$aux);
        $palabrasClave = " <NOUN>,<PRONOUN>,<ADJECTIVE>";
        $aux = str_replace($palabrasClave,"",$aux);
        $aux = $aux."," ;

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ','){
                $preposition = $preposition.$aux[$i];
            }else{
                array_push($arrayPreposition, $preposition);
                $preposition ="";
            }
        }

        $random=rand(0,count($arrayPreposition)-1);

        return $arrayPreposition[$random];
    }

    public function pronounRandom(){
        $arrayPronoun = [];
        $pronoun = "";

        $aux = str_replace("|",",", $this->pronoun);
        $aux = str_replace("PRONOUN: ","",$aux);
        $palabrasClave = " <NOUN>,<ADJECTIVE>\r\n";
        $aux = str_replace($palabrasClave,"",$aux);
        $aux = $aux."," ;

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ','){
                $pronoun = $pronoun.$aux[$i];
            }else{
                array_push($arrayPronoun, $pronoun);
                $pronoun ="";
            }
        }

        $random=rand(0,count($arrayPronoun)-1);

        return $arrayPronoun[$random];
    }

    public function crearLinea($argumento){
        $arrayComposicion = [];
        $linea = "";
        $aux = str_replace("|",",", $argumento);
        $aux = str_replace("LINE:","",$aux);
        $aux = str_replace("LINEBREAK","",$aux);
        $aux = str_replace(" $","",$aux);
        $aux = $aux."," ;
        $aux = trim($aux);

        for ($i = 0; $i < strlen($aux); $i++) {
            if ($aux[$i] != ','){
                $linea = $linea.$aux[$i];
            }else{
                array_push($arrayComposicion, $linea);
                $linea ="";
            }
        }

        return $arrayComposicion;
    }


    public function lineaPoema($arrayRegla, $cantidadLineas){
        $lineaGenerada = "";

        for ($j = 0; $j < $cantidadLineas; $j++) {
            for ($i = 0; $i < count($arrayRegla); $i++) {
                if ($arrayRegla[$i] == "<NOUN>"){
                    $auxNoun = $this->nounRandom();
                    $lineaGenerada = $lineaGenerada.$auxNoun." ";

                }else if ($arrayRegla[$i] == "<PREPOSITION>"){
                    $auxPreposition = $this->prepositionRandom();
                    $lineaGenerada = $lineaGenerada.$auxPreposition." ";

                }else if ($arrayRegla[$i] == "<PRONOUN>\r\n"){
                    $auxPronoun = $this->pronounRandom();
                    $lineaGenerada = $lineaGenerada.$auxPronoun."\n";
                }
            }
        }

        dd( $lineaGenerada);
    }
}
